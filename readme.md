# Welcome to DjDemo !

###  Goal - 

> Create a django demo which creates a custom user and extends it to
> have different profiles based on the usertype. For the demo purpose we
> shall use user types as Customer & Business Terms - User should not
> require special username, instead use email as usernasme

### Pre-Requisites - 

 - Installed Mariadb
 - Installed Python
 - Installed Pip
 - Installed django

## Step by Step Guide

1.  Create Database
	  

> CREATE DATABASE djdemo CHARACTER SET = 'utf8' COLLATE =
> 'utf8_general_ci';

2.  Create djdemo Project        
> django-admin startproject djdemo

3.  Created a Database Config File 
> db.cnf [ProjectDir/db.cnf]

4.  Configured Django database by replacing existing value of DATABASES var with the following

        DATABASES = {
            'default': {
                'ENGINE': 'django.db.backends.mysql',
                'OPTIONS': {
                    'read_default_file': "{path}/db.cnf".format(path=BASE_DIR),
                    'init_command': 'SET sql_mode=STRICT_TRANS_TABLES, innodb_strict_mode=1'
                },
            }
        }

5.  Created django app
> python manage.py startapp accounts

6.  Added accounts to 'INSTALLED_APPS' in settings.py
 
>     INSTALLED_APPS = [
>             'django.contrib.admin',
>             'django.contrib.auth',
>             'django.contrib.contenttypes',
>             'django.contrib.sessions',
>             'django.contrib.messages',
>             'django.contrib.staticfiles',
>             'accounts', #here
>         ]

   
7.  Created a Constants file in accounts app i.e constants.py
8.  Created Models in [accounts/models.py](models.py)  

> UserManager, User, CustomerProfile, BusinessProfile

9.  Create Admin specific tasks in accounts/admin.py 

> UserCreationForm, UserChangeForm, UserAdmin

10. Register the custom models with admin site         
		
>             admin.site.register(User, UserAdmin)
>             admin.site.register(BusinessProfile)
>             admin.site.register(CustomerProfile)

11. Created a post_save_user_model_receiver function in models.py
12. Connected the receiver with the post save signal 
        
> post_save.connect(post_save_user_model_receiver, sender=
> settings.AUTH_USER_MODEL)

13. Added Custom User details in settings.py
       
>  AUTH_USER_MODEL = 'accounts.User'

14. Make migrations - 

> python manage.py makemigrations

15. Migrate -

> python manage.py migrate

16. Create a superuser

> python manage.py createsuperuser

17. python manage.py runserver

18. Goto localhost:8000/admin
19. Login with superuser
20. Now try creating 2 types of users - customer & business from the admin interface