import sys

from django.contrib.auth.models import AbstractUser, UserManager as DjUserManager
from django.db import models
from django.utils.translation import gettext_lazy as _
from django.conf import settings
from django.db.models.signals import post_save

from accounts.constants import USER_TYPE_CHOICES, PROFILE_TITLE_CHOICES, ISO_3166_CODES

class UserManager(DjUserManager):
    
    def _create_user(self, email, password, utype, **extra_fields):
        """
        Create and save a user with the given email, and password.
        """
        if not email:
            raise ValueError('Email can not be empty')
        email = self.normalize_email(email)        
        user = self.model(email=email,utype=utype, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, email=None, password=None, utype=None, **extra_fields):
        extra_fields.setdefault('is_staff', False)
        extra_fields.setdefault('is_superuser', False)
        return self._create_user(email, password, utype, **extra_fields)

    def create_superuser(self, email=None, password=None, utype='Admin', **extra_fields):
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_superuser', True)

        if extra_fields.get('is_staff') is not True:
            raise ValueError('Superuser must have is_staff=True.')
        if extra_fields.get('is_superuser') is not True:
            raise ValueError('Superuser must have is_superuser=True.')

        return self._create_user(email, password, utype, **extra_fields)        

class User(AbstractUser):
    """
    Users within the Django authentication system are represented by this
    model.
    Email and password are required. Other fields are optional.
    """
    
    username = None
    
    #delattr(AbstractUser, 'username')
    delattr(AbstractUser, 'first_name')
    delattr(AbstractUser, 'last_name')

    email = models.EmailField(_('email address'), unique=True)
    utype = models.CharField(max_length=2, choices=USER_TYPE_CHOICES, default='Customer')
    objects = UserManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['utype',]
    
    def get_full_name(self):
        """
        Return the email address as the full name for the user.
        """
        
        return self.email

    def get_short_name(self):
        """Return the email address as a short name for the user."""
        return self.email
    
    class Meta(AbstractUser.Meta):
        swappable = 'AUTH_USER_MODEL'
        
        
class CustomerProfile(models.Model):
    
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    title =  models.CharField(max_length=5, choices=PROFILE_TITLE_CHOICES)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    mobile = models.CharField(max_length=13)
    
    #Customer Address
    address1 = models.CharField("Address line 1", max_length=1024,)
    address2 = models.CharField("Address line 2", max_length=1024,) 
    zip_code = models.CharField("ZIP / Postal code", max_length=12,)
    city = models.CharField("City", max_length=1024,)
    state = models.CharField("State", max_length=1024,)
    country = models.CharField("Country", max_length=3, choices=ISO_3166_CODES, )
    
    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s %s' % (self.title, self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        short_name = '%s %s' % (self.title, self.first_name)
        return self.short_name.strip()
    
    def __str__(self):
        return str(self.user.email)

class BusinessProfile(models.Model):
    
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    
    business_name = models.CharField(max_length=50)
    
    title =  models.CharField(max_length=5, choices=PROFILE_TITLE_CHOICES)
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    
    mobile = models.CharField(max_length=13)
    phone = models.CharField(max_length=13)
    
    opening_time = models.SmallIntegerField(null=True, blank=True)
    closing_time = models.SmallIntegerField(null=True, blank=True)
    
    #cust_capacity = models.IntegerField()
    
    #Business Address
    address1 = models.CharField("Address line 1", max_length=1024,)
    address2 = models.CharField("Address line 2", max_length=1024,) 
    zip_code = models.CharField("ZIP / Postal code", max_length=12,)
    city = models.CharField("City", max_length=1024,)
    state = models.CharField("State", max_length=1024,)
    country = models.CharField("Country", max_length=3, choices=ISO_3166_CODES, )
    
    def get_full_name(self):
        """
        Return the first_name plus the last_name, with a space in between.
        """
        full_name = '%s %s %s' % (self.title, self.first_name, self.last_name)
        return full_name.strip()

    def get_short_name(self):
        """Return the short name for the user."""
        short_name = '%s %s' % (self.title, self.first_name)
        return self.short_name.strip()
    
    def get_business_name(self):
        """Return the business name for the user."""        
        return self.business_name    
    
    def __str__(self):
        return str(self.user.email)


def post_save_user_model_receiver(sender, instance, created, *args, **kwargs):
    
    if instance and created:
        try:
            print(instance.utype)
            if instance.utype == 'C':
                CustomerProfile.objects.create(user=instance)
            elif instance.utype == 'B':                
                BusinessProfile.objects.create(user=instance)
        except Exception as e:
            print('Exception occured: %s' %e)

post_save.connect(post_save_user_model_receiver, sender= settings.AUTH_USER_MODEL)